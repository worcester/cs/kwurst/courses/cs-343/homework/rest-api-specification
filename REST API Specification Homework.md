# REST API Specification Homework

## Instructions

<<<<<<< HEAD:REST API Specification Homework.md
Fork the GuestInfoAPI from your Team's area (e.g. `https://gitlab.com/worcester/cs/cs-343-01-02-fall-2022/team-n/microservices-kit/guestinfoapi`) to your private GitLab subgroup of the CS 343 01 02 Fall 2023 group. It will look something like
`Worcester State University / Computer Science Department / CS 343 01 02 Fall 2023 / Students/ username`. **You will be making your changes to the files in your GuestInfoAPI fork for this assignment.**
=======
**You will be making your changes to the `specification/openapi.yaml` file in your GuestInfoBackend fork for this assignment.** `Worcester State University / Computer Science Department / CS 343 01 02 Fall 2024 / Students/ username / microservices-theaspantry / guestinfosystem / guestinfobackend`
>>>>>>> origin/main:HW4 RESTAPI Specification.md

**You do not need to fork the REST API Specification Homework repository to your private GitLab subgroup. You will not be submitting your homework through that repository.**

The current API describes the Thea's Pantry's guests collection. It stores information about individual guests visiting the pantry.

The Thea's Pantry system also contains an inventory collection. It stores the inventory of the pantry (as pounds of food).

For your homework assignment, you are going to add additional endpoints to the API for Inventory, as well as add endpoints for Assistance for Guests.

**All of your changes will be made to the `specification/openapi.yaml` file in your personal GuestInfoAPI fork.**

## *Base Assignment*

**Everyone must complete this portion of the assignment in a *Meets Specification* fashion.**

### Specify new REST API endpoints for `/inventory`

**Note: This is not the way that the Thea's Pantry system actually works. The InventorySystem is a completely separate subsystem from the GuestInfoSystem, with its own API, backend, and frontends. This is for the purposes of homework only so you do not need to fork and clone an additional project.**

1. `GET /inventory`
    * 200 Response, should return the number of pounds in the inventory
    * 500 Response, should return ServerError
2. `PATCH /inventory?change=`*pounds*`%wsuID=`*id*
    * **This is a *query parameter* which you have not seen before in an OpenAPI specification. Look at `guests/{wsuID}` and use the `parameters` block as an example, but with the following changes:**
        * Since this does not apply to all `inventory` paths, it should not appear directly under `/inventory`. Instead, it should appear under the `patch:` method.
        * There are two parameters.
        * They are`query` parameters, not a `path` parameters.
        * Both `query` parameters are required.
        * There is already a schema for `wsuID` - use it.
        * There needs to be a schema for `pounds` to use for both the parameter and the response. This is an integer. Find an existing schema in the API for an integer. Use that as an example to make a new schema for `pounds`.
    * 200 Response, should return the number of pounds in the inventory
    * 400 Response, if missing either change or wsuID
    * 500 Response, should return ServerError

### Specify a new REST API endpoint for `/guests/{wsuID}/assistance`

When you have a collection within another collection, you can specify that inner collection as a resource in the URL. `assistance` is a collection within each individual guest.

If we address `/guests/1234567/assistance` we are addressing the JSON object that that is stored inside the guest with the id 1234567:

```json
{
  "wsuID": "1234567",
  "resident": true,
  "zipCode": "01602",
  "unemployment": false,
  "assistance": {
    "socSec": false,
    "TANF": false,
    "finAid": false,
    "other": false,
    "SNAP": false,
    "WIC": false,
    "breakfast": false,
    "lunch": false,
    "SFSP": false
  },
  "guestAge": 42, 
  "numberInHousehold": 4
}
```

So, with the path `/guests/{wsuID}/assistance, we can use any of the HTTP methods` - `POST`, `GET`, `PUT`, `PATCH`, `DELETE`.

* `POST` and `DELETE` don't make sense as all guests must have the assistance information.
* `GET` makes sense if we want to get just the assistance information for a specific guest.
* `PUT` and `PATCH` also make sense because we may want to change just the assistance information for a specific guest.

1. Add the endpoint `PUT /guests/{wsuID}/assistance`
    * There will be a required response body, which is the full assistance object. It will replace the current assistance object in that guest.
    * 200 Response, should return the URL of the assistance object in that guest.
    * 404 Response if the guest with that ID does not exist.
    * 500 Response, should return ServerError

### Specifications

* You will need to edit `specification/openapi.yaml`
<<<<<<< HEAD:REST API Specification Homework.md
=======
  * Choose good names the `operationID`s
>>>>>>> origin/main:HW4 RESTAPI Specification.md
* You will need to define a schema for pounds.
* *Reuse as many existing refs as possible - only add new ones when absolutely needed!*

### Check your API specification and submit

1. View your API specification with the swagger previewer.
<<<<<<< HEAD:REST API Specification Homework.md
2. Validate your API specification by running `commands/lint.sh`.
3. Commit your changes with the commit message `Base Assignment`.
=======
2. Validate your API specification by running `bin/spectral.sh`.
3. Make a copy of `specification/openapi.yaml` in the top-level directory and rename it to `base.yaml`.**
4. Commit your changes with the commit message `Base Assignment`.
>>>>>>> origin/main:HW4 RESTAPI Specification.md

## *Intermediate Add-On*

**Complete this portion in a *Meets Specification* fashion to apply towards base course grades of C or higher**

* See the the Course Grade Determination table in the syllabus to see how many Intermediate Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received an Acceptable grade on.
* You may not need to submit one for this assignment.

### Specify new REST API filtering endpoints for `/guests`

1. `GET /guests?ageEQ=` - guestAge equal to
2. `GET /guests?ageLT=` - guestAge less than
3. `GET /guests?ageGT=` - guestAge greater than
4. `GET /guests?ageLTEQ=` - guestAge less than or equal to
5. `GET /guests?ageGTEQ=` - guestAge greater than or equal to

* All should be optional parameters.
* Currently, there is no way in OpenAPI to specify that only one can be used at a time. Instead, add a description for each parameter that explains that it cannot be used with any other parameters.
  * Logic in the backend server is where we would have to prevent the use of more than one parameter, and would return a 400 response.
* 200 Response, should return a list of all guests who match the filter value, which could be empty.
* 400 Response - BadRequest

### Specifications

* You will need to edit `specification/openapi.yaml`
<<<<<<< HEAD:REST API Specification Homework.md
=======
  * Choose good names for the `operationID`s
>>>>>>> origin/main:HW4 RESTAPI Specification.md
* *Reuse as many existing refs as possible - only add new ones when absolutely needed!*

### Check your API specification and submit

1. View your API specification with the swagger previewer.
<<<<<<< HEAD:REST API Specification Homework.md
2. Validate your API specification by running `commands/lint.sh`.
3. Commit your changes with the commit message `Intermediate Assignment`.
=======
2. Validate your API specification by running `bin/spectral.sh`.
3. Make a copy of `specification/openapi.yaml` in the top-level directory and rename it to `intermediate.yaml`.**
4. Commit your changes with the commit message `Intermediate Assignment`.
>>>>>>> origin/main:HW4 RESTAPI Specification.md

## *Advanced Add-On*

**Complete this portion in a *Meets Specification* fashion to apply towards base course grades of B or higher**

* See the the Course Grade Determination table in the syllabus to see how many Intermediate Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received an Acceptable grade on.
* You may not need to submit one for this assignment.

### Specify a new REST API endpoint for `/guests/{wsuID}/assistance`

In the Base assignment we specified `PUT /guests/{wsuID}/assistance` and required a complete new assistance object to be sent as the request body. That might be a pain if you are only changing one field in the assistance object.

1. Add the endpoint `PATCH /guests/{wsuID}/assistance` that will allow query parameters.
    * There will be 9 query parameters:
      * `?socSec=`
      * `?TANF=`
      * `?finAid=`
      * `?other=`
      * `?SNAP=`
      * `?WIC=`
      * `?breakfast=`
      * `?lunch=`
      * `?SFSP=`
    * They are all optional, you can use 1-9 of them in a call.
    * Currently, there is no way in OpenAPI to specify that at least one must be used. Instead, add a description for each parameter that explains that at least one must be used.
      * Logic in the backend server is where we would have to enforce the use of at least one parameter, and would return a 400 response.
    * 200 Response, should return the URL of the assistance object in that guest.
    * 400 Response - BadRequest
    * 404 Response if the guest with that ID does not exist.

### Specifications

* You will need to edit `specification/openapi.yaml`
<<<<<<< HEAD:REST API Specification Homework.md
=======
  * Choose good names for the `operationID`s
>>>>>>> origin/main:HW4 RESTAPI Specification.md
* *Reuse as many existing refs as possible - only add new ones when absolutely needed!*

### Check your API specification and submit

1. View your API specification with the swagger previewer.
<<<<<<< HEAD:REST API Specification Homework.md
2. Validate your API specification by running `commands/lint.sh`.
3. Commit your changes with the commit message `IAdvanced Assignment`.

---

&copy; 2023 Karl R. Wurst <karl@w-sts.com>
=======
2. Validate your API specification by running `bin/spectral.sh`.
3. Make a copy of `specification/openapi.yaml` in the top-level directory and rename it to `advanced.yaml`.**
4. Commit your changes with the commit message `Advanced Assignment`.

---

&copy; 2024 Karl R. Wurst <karl@w-sts.com>
>>>>>>> origin/main:HW4 RESTAPI Specification.md

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
